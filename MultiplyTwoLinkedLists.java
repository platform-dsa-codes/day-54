//{ Driver Code Starts
import java.util.*;
class Node
{
    int data;
    Node next;
    Node(int data){
        this.data=data;
        next=null;
    }
}

class MultiplyTwoLL{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int t=sc.nextInt();
		while(t-->0){
			Node head=null;
			Node phead=null;
			int n=sc.nextInt();
			while(n-->0){
				int n1=sc.nextInt();
				if(head==null)
				head=new Node(n1);
			else{
				addHead(head,n1);
			}
			}
			int m=sc.nextInt();
			while(m-->0){
				int n1=sc.nextInt();
				if(phead==null)
				phead=new Node(n1);
			else{
				addPhead(phead,n1);
			}
			}
		GfG g=new GfG();

System.out.println(g.multiplyTwoLists(head,phead));
		}
	}
    public static void addHead(Node node,int a) {
          if (node == null)
            return;
		if(node.next==null)
			node.next=new Node(a);
		else
			addHead(node.next,a);
	}
	public static void addPhead(Node node,int a){
		 if (node == null)
            return;
		if(node.next==null)
		node.next=new Node(a);
	else
		addPhead(node.next,a);
	}
	}
	
	
// } Driver Code Ends


/*Node is defined as
class Node
{
    int data; 
    Node next;
    Node(int data) {
        this.data=data;
        this.next = null;
    }
}*/

class GfG {
    /*You are required to complete this method */
    public long multiplyTwoLists(Node l1, Node l2) {
        // Define modulo value
        long MOD = 1000000007;
        
        // Convert linked lists into arrays in reverse order
        int[] num1 = convertToArray(l1);
        int[] num2 = convertToArray(l2);
        
        // Initialize the result array
        long[] result = new long[num1.length + num2.length];
        
        // Perform multiplication
        for (int i = 0; i < num1.length; i++) {
            for (int j = 0; j < num2.length; j++) {
                result[i + j] += (long) num1[i] * (long) num2[j];
                result[i + j] %= MOD;
            }
        }
        
        // Convert the result array into a long number
        long finalResult = 0;
        for (int i = 0; i < result.length; i++) {
            finalResult += result[i] * pow(10, i, MOD);
            finalResult %= MOD;
        }
        
        return finalResult;
    }
    
    // Helper function to convert a linked list to an array in reverse order
    private int[] convertToArray(Node head) {
        int count = 0;
        Node current = head;
        while (current != null) {
            count++;
            current = current.next;
        }
        
        int[] arr = new int[count];
        current = head;
        for (int i = count - 1; i >= 0; i--) {
            arr[i] = current.data;
            current = current.next;
        }
        return arr;
    }
    
    // Helper function to calculate (base^exp) % mod
    private long pow(long base, long exp, long mod) {
        long result = 1;
        while (exp > 0) {
            if (exp % 2 == 1) {
                result = (result * base) % mod;
            }
            base = (base * base) % mod;
            exp /= 2;
        }
        return result;
    }
}
